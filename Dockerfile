FROM python:3.9.7

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip3 install --no-cache-dir --requirement requirements.txt

COPY . .

ENV FLASK_APP=start.py

ENV TOKEN=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

CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]

EXPOSE 5000
