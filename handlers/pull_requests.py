import os
import requests

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """
    answer = []
    pagecount = 100
    item_page = 1
    keystate = {'state': state, 'per_page': pagecount, 'page': item_page}
    if (((state.find('bug')) != -1) or ((state.find('needs-review')) != -1)):
        urlissues = 'https://api.github.com/search/issues?q=is:pr%20label:{}%20repo:boto/boto3'
        url = urlissues.format(state)
        res = requests.get(url, headers=HEADERS)
        response = res.json()
        response = response['items']
    else:
        url = 'https://api.github.com/repos/boto/boto3/pulls'
        res = requests.get(url, headers=HEADERS, params=keystate)
        response = res.json()
    print(res.status_code)
    while 'next' in res.links.keys():
        keystate = {'state': state, 'per_page': pagecount, 'page': item_page}
        res=requests.get(url, headers=HEADERS, params=keystate)
        response.extend(res.json())
        item_page += 1
    for i in range(0, len(response)):
        data = response[i]
        if (((state.find('bug')) != -1) or ((state.find('needs-review')) != -1)):
            if data["state"].find('open') != -1 :
                answer.append({"title": data["title"], "num": data["number"], "link": data["url"]})
        else:
            answer.append({"title": data["title"], "num": data["number"], "link": data["url"]})
    return answer
